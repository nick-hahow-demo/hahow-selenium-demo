import os
import time
from dotenv import load_dotenv
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

load_dotenv("./.env")


def test_hahow_login(driver: Chrome):
    driver.get("https://hahow.in/")

    header_block = driver.find_element(By.CSS_SELECTOR, "div#header")
    login_button = header_block.find_elements(By.CSS_SELECTOR, "li")[-1]
    login_button.click()
    time.sleep(10)

    login_form = driver.find_element(
        By.XPATH, "/html/body/div[5]/div/div/div/div/div/form"
    )
    input_tag_list = login_form.find_elements(
        By.CSS_SELECTOR, "input"
    )

    # 預期會抓到兩個 input tag 第一個是帳號第二個是密碼
    input_tag_list[0].send_keys(os.getenv("ACCOUNT"))
    input_tag_list[1].send_keys(os.getenv("PASSWORD"))
    input_tag_list[1].send_keys(Keys.ENTER)
    time.sleep(10)

    my_cursor_button = driver.find_element(
        By.CSS_SELECTOR, "li.my-courses-desktop.marg-rl-5"
    )

    assert "我的學習" in my_cursor_button.text
